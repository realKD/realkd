package edu.uab.consapt.sampling;

public interface PotentialFunction<T> {

	public double getPotential(T t);

}
